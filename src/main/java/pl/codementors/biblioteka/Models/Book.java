package pl.codementors.biblioteka.Models;

public class Book {

        private String title;
        private String author;
        private int issueDate;

        public Book(){

        }

        public Book(String title, String author, int issueDate){
            this.title = title;
            this.author = author;
            this.issueDate = issueDate;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public int getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(int issueDate) {
            this.issueDate = issueDate;
        }
}
