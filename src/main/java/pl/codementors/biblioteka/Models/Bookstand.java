package pl.codementors.biblioteka.Models;

public class Bookstand {

    private Shelve[] storedShelvesNo = new Shelve[10];

    public void setStoredShelvesNo(int index, Shelve shelve) {

        storedShelvesNo[index] = shelve;
    }

    public Shelve getStoredShelvesNo(int index) {

        return storedShelvesNo[index];
    }
}
