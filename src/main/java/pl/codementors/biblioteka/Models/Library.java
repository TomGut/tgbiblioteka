package pl.codementors.biblioteka.Models;

public class Library {

    private Bookstand[] storedBookstandsNo = new Bookstand[10];

    public void setStoredBookstandsNo(int index, Bookstand bookstand) {

        storedBookstandsNo[index] = bookstand;
    }

    public Bookstand getStoredBookstandNo(int index) {

        return storedBookstandsNo[index];
    }

}
