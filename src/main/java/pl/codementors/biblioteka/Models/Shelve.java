package pl.codementors.biblioteka.Models;

public class Shelve {


    private Book[] storedBooks = new Book[10];

    public void setStoredBooks(int index, Book book) {
        storedBooks[index] = book;
    }

    public Book getStoredBooks(int index) {
        return storedBooks[index];
    }
}