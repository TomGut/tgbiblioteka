package pl.codementors.biblioteka;

import pl.codementors.biblioteka.Models.Book;
import pl.codementors.biblioteka.Models.Bookstand;
import pl.codementors.biblioteka.Models.Library;
import pl.codementors.biblioteka.Models.Shelve;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner inputScanner = new Scanner(System.in);

        Shelve shelve = new Shelve();
        Bookstand bookstand = new Bookstand();
        Library library = new Library();

        boolean running = true;

        while (running) {

            System.out.println("KATALOG KSIĄŻEK");
            System.out.println(" ");
            System.out.print("Czy chcesz dodać książkę ?\n0 - NIE I WYJŚCIE,\n1 - TAK\n2 - WYPISANIE KSIĄŻKI \n3 - WYPISZ WSZYSTKIE \n ");
            System.out.println(" ");

            int command = inputScanner.nextInt();

            switch (command) {
                case 1: {

                    System.out.println("Podaj tytuł książki");

                    String bookTitle = inputScanner.next();

                    System.out.println("Podaj autora");

                    String authorName = inputScanner.next();

                    System.out.println("Podaj rok wydania (tylko rok)");

                    int issueYear = inputScanner.nextInt();

                    Book book = new Book(bookTitle, authorName, issueYear);

                    System.out.print("\nTytuł: " + book.getTitle() + ", Autor: " + book.getAuthor() + ", Rok wydania: " + book.getIssueDate() + "\n");
                    System.out.println(" ");

                    System.out.println("Na który regał chcesz wrzucić książkę ?");

                    int bookstandIndex = inputScanner.nextInt();
                    library.setStoredBookstandsNo(bookstandIndex, bookstand);

                    System.out.println("Na którą półkę w regale chcesz wrzucić książkę ?");

                    int shelveIndex = inputScanner.nextInt();
                    bookstand.setStoredShelvesNo(shelveIndex, shelve);

                    System.out.println("Pod jakim indeksem wrzucić książkę na półkę ?");

                    int bookOnShelveIndex = inputScanner.nextInt();
                    shelve.setStoredBooks(bookOnShelveIndex, book);

                    System.out.println(" ");

                    break;
                }

                case 2: {

                    System.out.println("Podaj regał z którego chcesz wypisać książkę ");

                    int bookstandIndex = inputScanner.nextInt();

                    System.out.println("Podaj półkę z której chcesz wypisać książkę ");

                    int shelveIndex = inputScanner.nextInt();

                    System.out.println("Podaj miejsce na półce z którego chcesz wypisać książkę ");

                    int bookOnShelveIndex = inputScanner.nextInt();

                    System.out.println(" ");
                    System.out.print("Regał nr. " + bookstandIndex + ", Półka nr. " + shelveIndex + ", Miejsce na półce nr. " + bookOnShelveIndex + "\n");
                    System.out.println("Tytuł: " + library.getStoredBookstandNo(bookstandIndex).getStoredShelvesNo(shelveIndex).getStoredBooks(bookOnShelveIndex).getTitle());
                    System.out.println("Autor: " + library.getStoredBookstandNo(bookstandIndex).getStoredShelvesNo(shelveIndex).getStoredBooks(bookOnShelveIndex).getAuthor());
                    System.out.println("Data wydania: " + library.getStoredBookstandNo(bookstandIndex).getStoredShelvesNo(shelveIndex).getStoredBooks(bookOnShelveIndex).getIssueDate());
                    System.out.println(" ");

                    break;
                }

                case 3: {

                    for(int l=0; library.getStoredBookstandNo(l) ; l++) {

                        System.out.print("Regał nr. " + l + ", ");

                        for (int k = 0; bookstand.getStoredShelvesNo(k) != null; k++) {

                            System.out.print("Półka nr " + k + ", ");

                            for (int j = 0; shelve.getStoredBooks(j) != null; j++) {

                                System.out.print("Książka z indexem nr. " + j + " \n");
                                System.out.println("Tytuł: " + shelve.getStoredBooks(j).getTitle());

                            }
                        }

                        System.out.println(" ");
                    }

                    break;
                }

                case 0: {

                    System.out.print("\nOk do usłyszenia,\nWychodzę z programu.\n ");
                    running = false;

                    break;
                }

            }
        }
    }
}

